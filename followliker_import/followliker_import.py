import csv

def get_users():
    f = open('users.txt', 'r')
    users = f.read().splitlines()
    f.close()

    make_csv(users)

def make_csv(users):
    f = open('users.csv', 'w')
    writer = csv.writer(f)
    for user in users:
        writer.writerow([user, 'User\'s Photo'])
    f.close()

    print('CSV has successfully been made')

get_users()
